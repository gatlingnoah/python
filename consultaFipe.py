import json
import urllib.request


# https://deividfortuna.github.io/fipe/

# return code do nome
def reCode(tipo, nome):
    with urllib.request.urlopen(f"https://parallelum.com.br/fipe/api/v1/{tipo}/marcas") as url:
        data = json.loads(url.read().decode())
        for i in data:
            if nome == i['nome']:
                x = i['codigo']
                return x


# print(reCode(tipos[0], 'Toyota'))

# return o code do modelo
def reMode(t, cNome, modelo):
    with urllib.request.urlopen(f"https://parallelum.com.br/fipe/api/v1/{t}/marcas/{cNome}/modelos") as url:
        data = json.loads(url.read().decode())
        for i in data:
            for j in data[i]:
                if (j['nome']) == modelo:
                    x = j['codigo']
                    return x


# print(reMode("carros", "59", "Saveiro CL 1.6 Mi / CL/ C 1.6"))

# pegar ano
def reYear(t, cNome, cModelo, y):
    with urllib.request.urlopen(
            f"https://parallelum.com.br/fipe/api/v1/{t}/marcas/{cNome}/modelos/{cModelo}/anos") as url:
        data = json.loads(url.read().decode())
        for u in data:
            x = u['nome']
            if x[:4] == y:
                r = u['codigo']
                return r


def consultaPipe(typ, nome, model, yea):
    """https://parallelum.com.br/fipe/api/v1/carros/marcas/3/modelos/6/anos/1998-1"""

    with urllib.request.urlopen(
            f"https://parallelum.com.br/fipe/api/v1/{typ}/marcas/{nome}/modelos/{str(model)}/anos/{yea}") as url:
        data = json.loads(url.read().decode())
        for i in data:
            print(f"{i}: {data[i]}")


c = ["carros", "Alfa Romeo", "145 Elegant 2.0 16V", "1998"]

codeNome = reCode(c[0], c[1])
codeModel = reMode(c[0], codeNome, c[2])
codeYer = reYear(c[0], codeNome, codeModel, c[3])
print(consultaPipe(c[0], codeNome, codeModel, codeYer))
