# Tabalho de Algoritmo, Componentes
graph = [
    [6],
    [1, 2, 5],
    [2, 1, 5],
    [3, 4],
    [4, 3],
    [5, 1, 2, 7],
    [7, 5]
]

comp = 0
check = graph[0]

for vertex in graph:  # vertex all
    # print(f'\n Vertice: {vertex}')
    for num in vertex:  # number vertex
        # print(f'número da vertice: {num} ')
        if num not in check:
            for num1 in vertex: # percorre a vertex de novo pra não repetir comp
                check.append(num1)
            comp += 1

print(f'\nComponentes: {comp} \ncheck {check}')
'''
print(f'Quantidades de componentes: {comp}')
print(f'Primeiro: {first}')
print(f'Visitados: {visited}')
'''
jogador = dict()
partidas = list()

jogador['nome'] = str(input('Nome do jogador: '))
total = int(input(f'Quantas partidas o {jogador["nome"]} jogou? '))

for cont in range(0, total):
    partidas.append(int(input(f'    Quantos gols na partida {cont}? ')))
jogador['gols'] = partidas[:]
jogador['total'] = sum(partidas)

print('-=' * 30)
print(jogador)

print('-=' * 30)
for k,v in jogador.items():
    print(f'O Campo {k} tem o valor {v}')

print('-=' * 30)
print(f'O jogador {jogador["nome"]} jogou {len(jogador["gols"])} partidas')
for i, v in enumerate(jogador['gols']):
    print(f'    => Na partida {i}, fez {v} gols')

print(f'Foi um total de {jogador["total"]} gols')