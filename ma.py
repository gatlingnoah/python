

def menu():
    print('''\033[31m---------- Matriz adjacências ----------
     1 - Cria um Grafo!
     2 - Visualizar
     3 - Inserir
     4 - Visualizar Vértices adjacentes
     6 - Calcular o Grau
     7 - Sair \033[m
    ''')


x = 0
dist = []
nVertices = 0

while x != 7:
    menu()
    x = int(input('Informe a opcão: '))

    if x == 1:
        print("Criando Grafo..")
        nVertices = int(input('Números de Vértices ? '))

    elif x == 2:
        print(dist)

    elif x == 3:
        print("Inserir")
        for i in range(nVertices):
            dist.append([])
            for j in range(nVertices):
                valor = int(input('Arestas ? '))
                dist[i].append(valor)

    elif x == 4:
        print("Visualizar Vértices adjacentes")
    elif x == 6:
        print("Calcular o Grau")
    else:
        print('Saindo!!')

('\n'
 '17-02-2020 Lucas Natan Camacho\n'
 '\n'
 '* Elabore um programa que implemente funções para:\n'
 '1- Permita o armazenamento de um grafo com n vértices em forma de matriz de adjacências\n'
 '2- Visualizar os grafos (todas as arestas)\n'
 '3- Inserir Aresta\n'
 '4- Visualizar os vértices adjacentes de um determinado vértice\n'
 '5- Dado 2 vértices (v1 e v2) verificar a existência de uma aresta entre eles.\n'
 '6- Calcular o grau de um  vértice específico\n')
